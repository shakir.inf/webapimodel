﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Message;
//using System.Data.OracleClient;
//using System.Net.Sockets;

namespace DB_Manager
{
    public sealed class ADOControllerDefault
    {
        public ADOControllerDefault()
        {
        }

        string ConString = ConfigurationSettings.AppSettings["ConnectionString"];
        string ConStringImage = ConfigurationSettings.AppSettings["ConnectionStringImage"];

        //// --------------------- or --------------------
        //string ConString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;

        public DataSet Fill(OleDbCommand command)
        {
            DataSet DS = new DataSet();
            try
            {
                OleDbConnection con = new OleDbConnection(ConString);
                OleDbDataAdapter da = new OleDbDataAdapter(command);
                da.SelectCommand.Connection = con;

                con.Open();
                da.Fill(DS);
                con.Close();
                return DS;
            }
            catch (Exception ex)
            {
                DS = null;
                WriteErrorLog(ex);
                return DS;
            }
        }
        public bool ExecuteNonQuery(OleDbCommand command)
        {
            try
            {
                OleDbConnection con = new OleDbConnection(ConString);
                con.Open();
                command.Connection = con;
                command.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
                return false;
            }


        }
        public bool ExecuteNonQuery(OleDbConnection con, OleDbCommand command)
        {
            try
            {
                if (con.ConnectionString == "") con.ConnectionString = ConString;
                if (con.State != ConnectionState.Open)
                {
                    OleDbTransaction transaction = null;
                    con.Open();
                    transaction = con.BeginTransaction();
                    //transaction = con.BeginTransaction(IsolationLevel.ReadCommitted);

                    command.Connection = con;
                    command.Transaction = transaction;
                }

                command.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex1)
            {
                try
                {
                    command.Transaction.Rollback();
                    con.Close();
                }
                catch (Exception ex2)
                {
                    WriteErrorLog(ex2);
                    return false;
                }


                WriteErrorLog(ex1);
                return false;
            }


        }
        public string Fill_ImageUrl(OleDbCommand command)
        {
            string strBase64 = "";
            byte[] bytes;
            try
            {

                OleDbConnection con = new OleDbConnection(ConStringImage);
                OleDbDataAdapter da = new OleDbDataAdapter(command);
                da.SelectCommand.Connection = con;

                con.Open();
                bytes = (byte[])command.ExecuteScalar();
                con.Close();

                if (bytes != null) strBase64 = "data:Image/png;base64," + Convert.ToBase64String(bytes);
                return strBase64;
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
                return strBase64;
            }
        }
        public string ExecuteNonQuery_Image(OleDbCommand command)
        {

            try
            {
                OleDbConnection con = new OleDbConnection(ConStringImage);
                con.Open();
                command.Connection = con;
                command.ExecuteNonQuery();
                con.Close();
                return htmlMsg.success;
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex);
                return htmlMsg.failed;
            }


        }
        private void WriteErrorLog(Exception ex)
        {
            try
            {
                string filePath = ConfigurationSettings.AppSettings["ErrorLogPath"];

                StackFrame frame = new StackFrame(2); // Gets upto 2 level higher to get the DL method
                var method = frame.GetMethod();
                var className = method.DeclaringType.Name;
                var methodName = method.Name;

                string IMsg = "";
                try { IMsg += ex.InnerException.Message; }
                catch { }

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine(String.Format("{0}. Event : {1} > {2}",
                                                     DateTime.Now.ToString("dd-MMM-yyyy H:mm:ss tt"),
                                                     className,
                                                     methodName));
                    writer.WriteLine("Msg :" + ex.Message + IMsg);
                    if (IMsg != "") writer.WriteLine("Inner Msg : " + IMsg);
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------");
                }
            }
            catch { }
        }
        public void WriteEmailLog(string mailSubject, string senderEmail, string recieverEmail, string ccEmail)
        {
            try
            {
                string filePath = ConfigurationSettings.AppSettings["EmailErrorLogPath"];
                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine(DateTime.Now.ToString("dd-MMM-yyyy H:mm:ss tt") + "    Subject : " + mailSubject);
                    writer.WriteLine("Sender : " + senderEmail);
                    writer.WriteLine("Reciever : " + recieverEmail);
                    if (ccEmail != "") writer.WriteLine("CC : " + ccEmail);
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------");
                }
            }
            catch (Exception exp) { }
        }

    }
}
