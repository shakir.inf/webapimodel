﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Drawing;
using System.Web;
using System.IO;
using System.Configuration;
using System.Text;
using DB_Manager;
using System.Data.OleDb;
using System.Globalization;
using System.Diagnostics;


namespace WebAPIModel
{
    public class UtilUI
    {
        public static void WriteErrorLog(Exception ex)
        {
            try
            {
                string filePath = ConfigurationSettings.AppSettings["ErrorLogPath"];

                StackFrame frame = new StackFrame(2); // Gets upto 2 level higher to get the DL method
                var method = frame.GetMethod();
                var className = method.DeclaringType.Name;
                var methodName = method.Name;

                string IMsg = "";
                try { IMsg += ex.InnerException.Message; }
                catch { }

                using (StreamWriter writer = new StreamWriter(filePath, true))
                {
                    writer.WriteLine(String.Format("{0}. Event : {1} > {2}",
                                                     DateTime.Now.ToString("dd-MMM-yyyy H:mm:ss tt"),
                                                     className,
                                                     methodName));
                    writer.WriteLine("Msg :" + ex.Message + IMsg);
                    if (IMsg != "") writer.WriteLine("Inner Msg : " + IMsg);
                    writer.WriteLine("-----------------------------------------------------------------------------------------------------");
                }
            }
            catch { }
        }
        public static string ConvertIntoLocaleDate(DateTime SourceValue, string format)
        {
            string formattedDate = "";
            try
            {
                var cultureInfo = CultureInfo.CreateSpecificCulture("bn-BD"); //"bn_BD" means local culture/language of Bangladesh
                cultureInfo.DateTimeFormat.Calendar = new GregorianCalendar();

                formattedDate = SourceValue.ToString(format, cultureInfo);
                string[] numerals = cultureInfo.NumberFormat.NativeDigits;

                for (int n = 0; n < numerals.Length; n++)
                {
                    formattedDate = formattedDate.Replace(n.ToString(), numerals[n]);
                }
            }
            catch (Exception ex)
            { return format; }
            return formattedDate;
        }
        public static string ConvertIntoLocaleNumber(string Number)
        {
            try
            {
                var cultureInfo = CultureInfo.CreateSpecificCulture("bn-BD"); //"bn_BD" means local culture/language of Bangladesh
                string[] numerals = cultureInfo.NumberFormat.NativeDigits;

                for (int n = 0; n < numerals.Length; n++)
                {
                    Number = Number.Replace(n.ToString(), numerals[n]);
                }
            }
            catch (Exception ex)
            { }
            return Number;
        }
        public static string NativeToUnicode(string strValue)
        {
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                Byte[] encodedBytes = utf8.GetBytes(strValue);
                string str = encodedBytes[0].ToString();
                for (int i = 1; i < encodedBytes.Length; i++)
                {
                    str += "/" + encodedBytes[i];
                }
                return str;
            }
            catch (Exception ex)
            {
                return strValue;
            }
        }
        public static string UnicodeToNative(string strValue)
        {
            try
            {
                UTF8Encoding utf8 = new UTF8Encoding();
                string[] myString = strValue.Split('/');
                string str = strValue;
                if (myString.Length > 1)
                {
                    str = String.Empty;
                    byte[] myByteArray = new byte[myString.Length];
                    int i = 0;
                    foreach (string item in myString)
                    {
                        myByteArray[i++] = byte.Parse(item);
                    }

                    str = utf8.GetString(myByteArray);
                }
                return str;
            }
            catch (Exception ex)
            {
                return strValue;
            }
        }



        public static DataSet getInformationAny_Employee(DataStringDS stringDs)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet DS = new DataSet();
            ADOControllerDefault adoc = new ADOControllerDefault();

            if (stringDs.DataStrings[0].StringValue == "EmployeeInfo")
            {
                #region Get Employee Details
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeInfo");
                #endregion
            }
            else if (stringDs.DataStrings[0].StringValue == "EmployeeInfoByCode")
            {
                #region Get Employee Details
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeInfoByCode");
                cmd.Parameters["pEmployeeCode"].Value = stringDs.DataStrings[1].StringValue;

                #endregion
            }


            
            DS = adoc.Fill(cmd);
            return DS;
        }
        public static DataSet getInformationAny_Student(DataStringDS stringDs)
        {
            OleDbCommand cmd = new OleDbCommand();
            DataSet DS = new DataSet();
            ADOControllerDefault adoc = new ADOControllerDefault();

            if (stringDs.DataStrings[0].StringValue == "StudentInfo")
            {
                #region Get Employee Details
                cmd = DBCommandProvider.GetDBCommand("GetStudentInfo");
                #endregion
            }
            else if (stringDs.DataStrings[0].StringValue == "StudentInfoByCode")
            {
                #region Get Employee Details
                cmd = DBCommandProvider.GetDBCommand("GetStudentInfoByCode");
                cmd.Parameters["pStudentCode"].Value = stringDs.DataStrings[1].StringValue;

                #endregion
            }



            DS = adoc.Fill(cmd);
            return DS;
        }
    }
}