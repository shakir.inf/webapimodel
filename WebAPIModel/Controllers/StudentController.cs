﻿using DB_Manager;
using Message;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
//using System.Web.Http.Description;
using WebAPIModel.Models;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;



namespace WebAPIModel.Controllers
{
    public class StudentController : ApiController
    {
        //
        // GET: api/Student/
        public object Get()
        {

            try
            {
                DataStringDS stringDS = new DataStringDS();
                stringDS.DataStrings.AddDataStringsRow("StudentInfo");
                stringDS.AcceptChanges();

                DataSet DS = UtilUI.getInformationAny_Student(stringDS);

                List<Student> student = new List<Student>();
                foreach (DataRow row in DS.Tables[0].Rows)
                {
                    Student item = new Student();
                    item.StudentCode = row["StudentCode"].ToString();
                    item.StudentName = row["StudentName"].ToString();
                    student.Add(item);
                }

                //return JsonConvert.SerializeObject(student);
                return student;

            }
            catch
            {
                return "Error Occurred. Data Not Found.";
            }
        }

        //
        // GET: api/Student/5
       //[ResponseType(typeof(Student))]
        public object Get(string id)
        {

            try
            {
                DataStringDS stringDS = new DataStringDS();
                stringDS.DataStrings.AddDataStringsRow("StudentInfoByCode");
                stringDS.DataStrings.AddDataStringsRow(id);
                stringDS.AcceptChanges();

                DataSet DS = UtilUI.getInformationAny_Student(stringDS);
                
                List<Student> students = new List<Student>();
                Student student = new Student();
                if (DS.Tables[0].Rows.Count == 1)
                {
                    student.StudentID = Convert.ToInt32(DS.Tables[0].Rows[0]["StudentID"]);
                    student.StudentCode = DS.Tables[0].Rows[0]["StudentCode"].ToString();
                    student.StudentName = DS.Tables[0].Rows[0]["StudentName"].ToString();
                    student.DateOfBirth = Convert.ToDateTime(DS.Tables[0].Rows[0]["DateOfBirth"].ToString());
                    student.TotalPaidAmount = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalPaidAmount"].ToString());
                    student.Address = DS.Tables[0].Rows[0]["Address"].ToString();
                    student.Active = Convert.ToInt32(DS.Tables[0].Rows[0]["Active"].ToString());

                    

                    //return JsonConvert.SerializeObject(student,Formatting.None);

                    string json = new JavaScriptSerializer().Serialize(student);
                    dynamic jsonD = JsonConvert.DeserializeObject(json);
                    return jsonD;


                }
                else return null;


            }
            catch (Exception ex)
            {
                return "Error Occurred. Data Not Found.";
            }
        }

        //
        // POST: api/Student
        public string Post([FromBody] Student student)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_STUDENT_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("pStudentCode", student.StudentCode);
                cmd.Parameters.AddWithValue("pStudentName", student.StudentName);
                cmd.Parameters.AddWithValue("pDateOfBirth", student.DateOfBirth);
                cmd.Parameters.AddWithValue("pTotalPaidAmount", student.TotalPaidAmount);
                cmd.Parameters.AddWithValue("pAddress", student.Address);
                cmd.Parameters.AddWithValue("pActive", student.Active);
               
                ADOControllerDefault adoc = new ADOControllerDefault();
                if (adoc.ExecuteNonQuery(cmd)) return htmlMsg.success;
                else return htmlMsg.failed;
            }
            catch { return htmlMsg.failed; }

        }

        // PUT api/Student/5
        public string Put(string id, [FromBody] Student student)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_STUDENT_UPD";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("pStudentCode", id);
                cmd.Parameters.AddWithValue("pStudentName", student.StudentName);
                cmd.Parameters.AddWithValue("pDateOfBirth", student.DateOfBirth);
                cmd.Parameters.AddWithValue("pTotalPaidAmount", student.TotalPaidAmount);
                cmd.Parameters.AddWithValue("pAddress", student.Address);
                cmd.Parameters.AddWithValue("pActive", student.Active);

                ADOControllerDefault adoc = new ADOControllerDefault();
                if (adoc.ExecuteNonQuery(cmd)) return htmlMsg.success;
                else return htmlMsg.failed;
            }
            catch { return htmlMsg.failed; }
        }

        // DELETE api/Student/5
        public string Delete(string id)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_STUDENT_DEL";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("pStudentCode", id);

                ADOControllerDefault adoc = new ADOControllerDefault();
                if (adoc.ExecuteNonQuery(cmd)) return htmlMsg.success;
                else return htmlMsg.failed;
            }
            catch { return htmlMsg.failed; }
        }
    }
}
