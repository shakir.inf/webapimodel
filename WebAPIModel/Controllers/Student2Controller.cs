﻿using DB_Manager;
using Message;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Web.Mvc;

namespace WebAPIModel.Controllers
{
    public class Student2Controller : Controller
    {
        public class Student
        {
            public Int32 StudentID { get; set; }
            public string StudentCode { get; set; }
            public string StudentName { get; set; }

            public DateTime DateOfBirth { get; set; }
            public decimal TotalPaidAmount { get; set; }
            public string Address { get; set; }
            public Int32 Active { get; set; }
        }

        //
        // GET: /Student2/
        public string Index()
        {
            try
            {
                DataStringDS stringDS = new DataStringDS();
                stringDS.DataStrings.AddDataStringsRow("StudentInfo");
                stringDS.AcceptChanges();

                DataSet DS = UtilUI.getInformationAny_Student(stringDS);

                if (DS.Tables[0].Rows.Count > 0) return JsonConvert.SerializeObject(DS.Tables[0]);
                else return "Data Not Found.";

            }
            catch
            {
                return "Error Occurred. Data Not Found.";
            }
        }

        //
        // GET: /Student2/Details/5
        public string Details(string id)
        {
            try
            {
                DataStringDS stringDS = new DataStringDS();
                stringDS.DataStrings.AddDataStringsRow("StudentInfoByCode");
                stringDS.DataStrings.AddDataStringsRow(id);
                stringDS.AcceptChanges();

                DataSet DS = UtilUI.getInformationAny_Student(stringDS);



                //string ss = JsonConvert.SerializeObject(DS.Tables[0]);
                //var bb = JsonConvert.DeserializeObject(ss);

                if (DS.Tables[0].Rows.Count > 0) return JsonConvert.SerializeObject(DS.Tables[0]);
                else return "Data Not Found.";

            }
            catch
            {
                return "Error Occurred. Data Not Found.";
            }
        }

        //
        // GET: /Student2/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Student2/Create
        [HttpPost]
        public string Create(Student student)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_STUDENT_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("pStudentCode", student.StudentCode);
                cmd.Parameters.AddWithValue("pStudentName", student.StudentName);
                cmd.Parameters.AddWithValue("pDateOfBirth", student.DateOfBirth);
                cmd.Parameters.AddWithValue("pTotalPaidAmount", student.TotalPaidAmount);
                cmd.Parameters.AddWithValue("pAddress", student.Address);
                cmd.Parameters.AddWithValue("pActive", student.Active);

                ADOControllerDefault adoc = new ADOControllerDefault();
                if (adoc.ExecuteNonQuery(cmd)) return htmlMsg.success;
                else return htmlMsg.failed;
            }
            catch { return htmlMsg.failed; }
        }

        //
        // GET: /Student2/Edit/5
        public string Edit(string id)
        {
            try
            {
                DataStringDS stringDS = new DataStringDS();
                stringDS.DataStrings.AddDataStringsRow("StudentInfoByCode");
                stringDS.DataStrings.AddDataStringsRow(id);
                stringDS.AcceptChanges();

                DataSet DS = UtilUI.getInformationAny_Student(stringDS);


                if (DS.Tables[0].Rows.Count > 0) return JsonConvert.SerializeObject(DS.Tables[0]);
                else return "Data Not Found.";

            }
            catch
            {
                return "Error Occurred. Data Not Found.";
            }
        }

        //
        // POST: /Student2/Edit/5
        [HttpPost]
        public string Edit(string id, Student student)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_STUDENT_UPD";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("pStudentCode", id);
                cmd.Parameters.AddWithValue("pStudentName", student.StudentName);
                cmd.Parameters.AddWithValue("pDateOfBirth", student.DateOfBirth);
                cmd.Parameters.AddWithValue("pTotalPaidAmount", student.TotalPaidAmount);
                cmd.Parameters.AddWithValue("pAddress", student.Address);
                cmd.Parameters.AddWithValue("pActive", student.Active);

                ADOControllerDefault adoc = new ADOControllerDefault();
                if (adoc.ExecuteNonQuery(cmd)) return htmlMsg.success;
                else return htmlMsg.failed;
            }
            catch { return htmlMsg.failed; }
        }

        ////
        //// GET: /Student2/Delete/5
        //public string Delete(string id)
        //{
        //    try
        //    {
        //        DataStringDS stringDS = new DataStringDS();
        //        stringDS.DataStrings.AddDataStringsRow("StudentInfoByCode");
        //        stringDS.DataStrings.AddDataStringsRow(id);
        //        stringDS.AcceptChanges();

        //        DataSet DS = UtilUI.getInformationAny_Student(stringDS);

        //        if (DS.Tables[0].Rows.Count > 0) return JsonConvert.SerializeObject(DS.Tables[0]);
        //        else return "Data Not Found.";

        //    }
        //    catch
        //    {
        //        return "Error Occurred. Data Not Found.";
        //    }
        //}

        //
        // Delete: /Student2/Delete/5
        [HttpDelete]
        public string Delete(string id)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_STUDENT_DEL";
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("pStudentCode", id);

                ADOControllerDefault adoc = new ADOControllerDefault();
                if (adoc.ExecuteNonQuery(cmd)) return htmlMsg.success;
                else return htmlMsg.failed;
            }
            catch { return htmlMsg.failed; }
        }
    }
}
