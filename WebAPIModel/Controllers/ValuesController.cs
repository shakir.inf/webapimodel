﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPIModel.Controllers
{
    public class ValuesController : ApiController
    {        
        string ConString = ConfigurationSettings.AppSettings["ConnectionString"];

        public class Student
        {
            public int StudentID { get; set; }
            public string StudentName { get; set; }
        }

        // GET api/values
        public string Get()
        {
            //return new string[] { "value1", "value2" };
            DataSet DS = new DataSet();
            OleDbCommand command = new OleDbCommand("Select * From Student");

            OleDbConnection con = new OleDbConnection(ConString);
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            da.SelectCommand.Connection = con;

            con.Open();
            da.Fill(DS);
            con.Close();

            if (DS.Tables[0].Rows.Count > 0) return JsonConvert.SerializeObject(DS.Tables[0]);
            else return "Data Not Found";

        }

        // GET api/values/5
        public string Get(int id)
        {
            //return new string[] { "value1", "value2" };

            DataSet DS = new DataSet();
            OleDbCommand command = new OleDbCommand("select * From Student Where StudentID = " + id);

            OleDbConnection con = new OleDbConnection(ConString);
            OleDbDataAdapter da = new OleDbDataAdapter(command);
            da.SelectCommand.Connection = con;

            con.Open();
            da.Fill(DS);
            con.Close();

            if (DS.Tables[0].Rows.Count > 0) return JsonConvert.SerializeObject(DS.Tables[0]);
            else return "Data Not Found";
        }

        // POST api/values
        public string Post([FromBody] Student student)
        {
            OleDbCommand cmd = new OleDbCommand("Insert Into Student(StudentID, StudentName) Values(" + student.StudentID + ",'" + student.StudentName + "')");      
            OleDbConnection con = new OleDbConnection(ConString);

            con.Open();
            cmd.Connection = con;
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i == 1) { return "Saved Successfully " + student.StudentName; }
            else { return "Try again, Not Saved"; }

        }

        // PUT api/values/5
        public string Put(int id, [FromBody] string value)
        {
           
            OleDbCommand cmd = new OleDbCommand("Update Student Set StudentName = '" + value + "' Where StudentID = " + id);
            OleDbConnection con = new OleDbConnection(ConString);

            con.Open();
            cmd.Connection = con;
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i == 1) { return "Saved Successfully " + value; }
            else { return "Try again, Not Saved"; }

        }

        // DELETE api/values/5
        public string Delete(int id)
        {
            OleDbCommand cmd = new OleDbCommand("Delete From Student Where StudentID = " + id);
            OleDbConnection con = new OleDbConnection(ConString);

            con.Open();
            cmd.Connection = con;
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i == 1) { return "Saved Successfully " + id; }
            else { return "Try again, Not Saved"; }
        }
    }
}
