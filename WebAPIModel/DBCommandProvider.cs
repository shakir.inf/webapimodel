﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Configuration;
using System.IO;
using System.Data.OracleClient;
using Npgsql;

namespace WebAPIModel
{
    public class DBCommandProvider
    {
        static private DBCommandDS m_cmdDS = null; //new DBCommandDS();

        public DBCommandProvider()
        {


        }

        static public void WriteCommandsToFile(string fileFullPath)
        {
            if (m_cmdDS == null)
            {
                return;
            }
            m_cmdDS.WriteXml(fileFullPath);
        }

        static public OleDbCommand GetDBCommand(string sCmdName)
        {
            if (m_cmdDS == null)
            {
                bool bOk = readCommandFile();

                if (bOk == false)
                {
                    return null;
                }
            }

            DBCommandDS.DBCommand cmd = m_cmdDS.DBCommands.FindByCommandName(sCmdName);
            if (cmd == null)
            {
                AppLogger.LogFatal("Requested DbCommand '" + sCmdName + "' not found.");
            }
            return buildCommand(cmd);

        }  // end of GetDBCommand method

        static private bool readCommandFile()
        {
            string sCommandFilesDir = ConfigurationSettings.AppSettings["CommandFilesDir"];
            Debug.Assert(sCommandFilesDir.Length > 0);

            DirectoryInfo di = new DirectoryInfo(sCommandFilesDir);
            Debug.Assert(di.Exists == true, "Directory for commands file not found");

            m_cmdDS = new DBCommandDS();
            try
            {
                // Get only files that contain the letter "*.xml" pattern
                FileInfo[] files = di.GetFiles("*.xml");
                AppLogger.LogInfo("Number of command files found: " + files.Length);
                foreach (DataTable t in m_cmdDS.Tables)
                {
                    t.BeginLoadData();
                }

                // Count all the files in each subdirectory that contain the letter "e."
                foreach (FileInfo fiNext in files)
                {
                    AppLogger.LogInfo("Reading command file: " + fiNext.FullName);
                    m_cmdDS.ReadXml(fiNext.FullName, XmlReadMode.IgnoreSchema);
                }


                string sBinDir = ConfigurationSettings.AppSettings["BinDir"];
                sBinDir = (sBinDir == null ? String.Empty : sBinDir.Trim());

                if (Directory.Exists(sBinDir) == false)
                {
                    AppLogger.LogFatal("Could not find bin directory.");
                    m_cmdDS = null;
                    return false;
                }
                string sComandFile = String.Empty;
                try
                {
                    sComandFile = Path.Combine(sBinDir, "m_cmdDS.xml");
                }
                catch //(Exception ex)
                {
                    AppLogger.LogFatal("Could not find bin directory.");
                    m_cmdDS = null;
                    return false;
                }

                //m_cmdDS.WriteXml(sComandFile);



                foreach (DataTable t in m_cmdDS.Tables)
                {
                    t.EndLoadData();
                }

            }
            catch (Exception ex)
            {
                AppLogger.LogFatal("Could not read command files.", ex);
                m_cmdDS = null;
                return false;
            }

            AppLogger.LogInfo("Total number of commands found: " + m_cmdDS.DBCommands.Count);
            m_cmdDS.EnforceConstraints = true;

            return true;
        } // end of readCommandFile method

        static public OleDbCommand buildCommand(DBCommandDS.DBCommand cmd)
        {

            OleDbCommand oleCmd = new OleDbCommand();

            oleCmd.CommandText = cmd.CommandText;
            oleCmd.CommandTimeout = cmd.CommandTimeout;
            oleCmd.CommandType = (CommandType)Enum.Parse(typeof(CommandType), cmd.CommandType, true);

            DBCommandDS.Parameter[] parameters = cmd.GetParameters();

            int nParamCount = parameters.GetLength(0);

            if (nParamCount == 0)
            {
                return oleCmd;
            }

            DBCommandDS.Param[] pr = parameters[0].GetParams();

            foreach (DBCommandDS.Param prNext in pr)
            {
                OleDbParameter param = new OleDbParameter();
                param.ParameterName = prNext.ParameterName;
                try
                {
                    param.SourceColumn = prNext.ParameterSourceColumn;
                }
                catch
                {
                    param.SourceColumn = "";
                }

                try
                {
                    param.DbType = (DbType)Enum.Parse(typeof(DbType), prNext.ParameterType, true);
                }
                catch
                {
                    param.DbType = DbType.String;  // set to default value
                }
                try
                {
                    param.Direction = (ParameterDirection)Enum.Parse(typeof(ParameterDirection), prNext.ParameterDirection, true);
                }
                catch
                {
                    param.Direction = ParameterDirection.Input;  // set to default value
                }

                try
                {
                    param.SourceVersion = (DataRowVersion)Enum.Parse(typeof(DataRowVersion), prNext.ParameterSourceVersion, true);
                }
                catch
                {
                    param.SourceVersion = DataRowVersion.Current;  // set to default value
                }
                oleCmd.Parameters.Add(param);
            }
            return oleCmd;
        }  // end of buildCommand method      

        static public OracleCommand GetDBCommand_Ora(string sCmdName)
        {
            if (m_cmdDS == null)
            {
                bool bOk = readCommandFile();

                if (bOk == false)
                {
                    return null;
                }
            }

            DBCommandDS.DBCommand cmd = m_cmdDS.DBCommands.FindByCommandName(sCmdName);
            if (cmd == null)
            {
                AppLogger.LogFatal("Requested DbCommand '" + sCmdName + "' not found.");
            }
            return buildCommand_Ora(cmd);

        }  // end of GetDBCommand method

        static public OracleCommand buildCommand_Ora(DBCommandDS.DBCommand cmd)
        {

            OracleCommand oleCmd = new OracleCommand();

            oleCmd.CommandText = cmd.CommandText;
            oleCmd.CommandTimeout = cmd.CommandTimeout;
            oleCmd.CommandType = (CommandType)Enum.Parse(typeof(CommandType), cmd.CommandType, true);

            DBCommandDS.Parameter[] parameters = cmd.GetParameters();

            int nParamCount = parameters.GetLength(0);

            if (nParamCount == 0)
            {
                return oleCmd;
            }

            DBCommandDS.Param[] pr = parameters[0].GetParams();

            foreach (DBCommandDS.Param prNext in pr)
            {
                OracleParameter param = new OracleParameter();
                param.ParameterName = prNext.ParameterName;
                try
                {
                    param.SourceColumn = prNext.ParameterSourceColumn;
                }
                catch
                {
                    param.SourceColumn = "";
                }

                try
                {
                    param.DbType = (DbType)Enum.Parse(typeof(DbType), prNext.ParameterType, true);
                }
                catch
                {
                    param.DbType = DbType.String;  // set to default value
                }
                try
                {
                    param.Direction = (ParameterDirection)Enum.Parse(typeof(ParameterDirection), prNext.ParameterDirection, true);
                }
                catch
                {
                    param.Direction = ParameterDirection.Input;  // set to default value
                }

                try
                {
                    param.SourceVersion = (DataRowVersion)Enum.Parse(typeof(DataRowVersion), prNext.ParameterSourceVersion, true);
                }
                catch
                {
                    param.SourceVersion = DataRowVersion.Current;  // set to default value
                }
                oleCmd.Parameters.Add(param);
            }
            return oleCmd;
        }  // end of buildCommand method 

        static public NpgsqlCommand GetDBCommand_PG(string sCmdName)
        {
            if (m_cmdDS == null)
            {
                bool bOk = readCommandFile();

                if (bOk == false)
                {
                    return null;
                }
            }

            DBCommandDS.DBCommand cmd = m_cmdDS.DBCommands.FindByCommandName(sCmdName);
            if (cmd == null)
            {
                AppLogger.LogFatal("Requested DbCommand '" + sCmdName + "' not found.");
            }
            return buildCommand_PG(cmd);

        }  // end of GetDBCommand method

        static public NpgsqlCommand buildCommand_PG(DBCommandDS.DBCommand cmd)
        {

            NpgsqlCommand oleCmd = new NpgsqlCommand();

            oleCmd.CommandText = cmd.CommandText;
            oleCmd.CommandTimeout = cmd.CommandTimeout;
            oleCmd.CommandType = (CommandType)Enum.Parse(typeof(CommandType), cmd.CommandType, true);

            DBCommandDS.Parameter[] parameters = cmd.GetParameters();

            int nParamCount = parameters.GetLength(0);

            if (nParamCount == 0)
            {
                return oleCmd;
            }

            DBCommandDS.Param[] pr = parameters[0].GetParams();

            foreach (DBCommandDS.Param prNext in pr)
            {
                NpgsqlParameter param = new NpgsqlParameter();
                param.ParameterName = prNext.ParameterName;
                try
                {
                    param.SourceColumn = prNext.ParameterSourceColumn;
                }
                catch
                {
                    param.SourceColumn = "";
                }

                try
                {
                    param.DbType = (DbType)Enum.Parse(typeof(DbType), prNext.ParameterType, true);
                }
                catch
                {
                    param.DbType = DbType.String;  // set to default value
                }
                try
                {
                    param.Direction = (ParameterDirection)Enum.Parse(typeof(ParameterDirection), prNext.ParameterDirection, true);
                }
                catch
                {
                    param.Direction = ParameterDirection.Input;  // set to default value
                }

                try
                {
                    param.SourceVersion = (DataRowVersion)Enum.Parse(typeof(DataRowVersion), prNext.ParameterSourceVersion, true);
                }
                catch
                {
                    param.SourceVersion = DataRowVersion.Current;  // set to default value
                }
                oleCmd.Parameters.Add(param);
            }
            return oleCmd;
        }  // end of buildCommand method  

    }  // end of class
}