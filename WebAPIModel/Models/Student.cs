﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIModel.Models
{
    public class Student
    {
        public Int32 StudentID { get; set; }
        public string StudentCode { get; set; }
        public string StudentName { get; set; }

        public DateTime DateOfBirth { get; set; }
        public decimal TotalPaidAmount { get; set; }
        public string Address { get; set; }
        public Int32 Active { get; set; }
    }

    

}
      