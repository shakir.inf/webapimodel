﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Windows.Forms;

namespace Message
{
    class Msg
    {
    }

    public static class htmlMsg
    {
        public static string invalidUserID = "<b><font color=\"#FF0000\" size=2px>"
                                            + "Invalid Login ID."
                                            + "</font></b>";

        public static string invalidPassword = "<b><font color=\"#FF0000\" size=2px>"
                                                + "Invalid Password."
                                                + "</font></b>";

        public static string dbConnectionNotOpen = "<b><font color=\"#FF0000\"  size=2px>"
                                                + "Database Connection not open."
                                                + "</font></b>";

        public static string success = "Operation Successful.";


        public static string failed = "Sorry, Operation Failed.";

        public static string invalidInput = "<b><font color=\"#FF0000\"  size=2px>"
                            + "Your input is not valid."
                            + "</font></b>";

        public static string invalidAmount = "<b><font color=\"#FF0000\"  size=2px>"
                    + "Input valid amount."
                    + "</font></b>";

        public static string inputIsRequired = "<b><font color=\"#FF0000\"  size=2px>"
            + "Input is Required."
            + "</font></b>";

        public static string selectItem = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Please, Select an item"
                                    + "</font></b>";

        public static string selectEmployee = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Please, Select an Employee"
                                    + "</font></b>";


        public static string dataNotFound = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Sorry, data not found."
                                    + "</font></b>";

        public static string dataExist = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Sorry, Data already exist."
                                    + "</font></b>";

        public static string dataNotExist = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Sorry, Data not exist."
                                    + "</font></b>";

        public static string insufficientBalance = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Insufficient Balance."
                                    + "</font></b>";

        public static string underDevelopment = "<b><font color=\"orange\" size=2px>"
                                            + "Under Development"
                                            + "</font></b>";

        public static string inputEmpCode = "<b><font color=\"#FF0000\"  size=2px>"
            + "Input Employee Code."
            + "</font></b>";

        public static string viewEmpDetails = "<b><font color=\"#FF0000\"  size=2px>"
            + "View Employee Details."
            + "</font></b>";


        public static string dateFromDateToErr = "<b><font color=\"#FF0000\"  size=2px>"
            + "&ldquo;Date From&rdquo; should not be greate then &ldquo;Date To&rdquo;"
            + "</font></b>";

        public static string actionExistErr = "<b><font color=\"#FF0000\"  size=2px>"
             + "Action already exists in the duration"
             + "</font></b>";

        public static string selectActivity = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Please, select an activity."
                                    + "</font></b>";


        public static string selectEmployeeLeft = "<b><font color=\"#FF0000\"  size=2px>"
                                    + "Please, Select an Employee"
                                    + "</font></b>";

        public static string dbConnectionErro = "<b><font color=\"#FF0000\" size=2px>"
                                            + "Dependant Database Connection not found."
                                            + "</font></b>";

        public static string invalidDateInput = "<b><font color=\"#FF0000\"  size=2px>"
                            + "Input valid Date"
                            + "</font></b>";

        public static string searchAgain = "<b><font color=\"#FF0000\"  size=2px>"
                            + "Search again, please"
                            + "</font></b>";

        public static string invalidAge = "<b><font color=\"#FF0000\"  size=2px>"
                            + "Input valid Age"
                            + "</font></b>";

        public static string submited = "<b><font color=\"Green\"  size=2px>"
                            + "Your data has submitted."
                            + "</font></b>";



    }

    public static class winMessage
    {
        public static string success = "Operation Successful.";
        public static string failed = "Operation Failed.";
        public static string invalidInput = "Your input is not valid.";
        public static string invalidUserID = "Invalid User ID.";
        public static string invalidPassword = "Invalid Password.";
        public static string inputIsRequired = "Invalid User ID.";
        public static string notPermitted = "Sorry, You are not permitted for the action";

    }

}
